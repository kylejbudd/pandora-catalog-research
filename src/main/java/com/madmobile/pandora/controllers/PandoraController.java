package com.madmobile.pandora.controllers;

import com.madmobile.pandora.dto.ProductItemDto;
import com.madmobile.pandora.services.PandoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


@Controller
public class PandoraController {
	
	private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private static final String US = Locale.US.toLanguageTag();
	private static final String GB = Locale.UK.toLanguageTag();

	@Autowired
	private PandoraService pandoraService;

	@RequestMapping("/kwiData")
	public ResponseEntity<Set<ProductItemDto>> getAllKWIDto() throws Exception {
		logger.log(Level.INFO, "Returning all the KWIDto products");
		return new ResponseEntity<>(pandoraService.getAllKWIProducts(), HttpStatus.OK);
	}

	@RequestMapping(value = "/d365Data", method = RequestMethod.GET)
	public ResponseEntity<Set<ProductItemDto>> getAllD365Dto() throws Exception {
		logger.log(Level.INFO, "Returning all the D365Dto products");
		return new ResponseEntity<>(pandoraService.getAllD365DtoProduct(), HttpStatus.OK);
	}

	@RequestMapping(value = "/eCommData-us", method = RequestMethod.GET)
	public ResponseEntity<Set<ProductItemDto>> getAllOCAPIDtoUS() throws Exception {
		logger.log(Level.INFO, "Returning all the OCAPIDto products for en-US");
		return new ResponseEntity<>(pandoraService.getAllOcapiProducts(US), HttpStatus.OK);
	}

	@RequestMapping(value = "/eCommData-gb", method = RequestMethod.GET)
	public ResponseEntity<Set<ProductItemDto>> getAllOCAPIDtoEU() throws Exception {
		logger.log(Level.INFO, "Returning all the OCAPIDto products for en-GB");
		return new ResponseEntity<>(pandoraService.getAllOcapiProducts(GB), HttpStatus.OK);
	}

	@GetMapping("/compareKwiAndOcapi")
	public String compareKwiAndOcapi(Model model) {
		logger.log(Level.INFO, "Comparing KWI and OCAPI");

		Set<ProductItemDto> kwiProducts = pandoraService.getAllKWIProducts();
		Set<ProductItemDto> ocapiProducts = pandoraService.getAllOcapiProducts(US);

		pandoraService.createExcelFile(ocapiProducts, kwiProducts, "eComm_KWI_Compare");

		return "/compareKwiAndOcapi";
 	}

	@GetMapping("/compareD365AndOcapi")
	public String compareD365AndOcapi(Model model) {
		logger.log(Level.INFO, "Comparing D365 and OCAPI");

		Set<ProductItemDto> d365Products = pandoraService.getAllD365DtoProduct();
		Set<ProductItemDto> ocapiProducts = pandoraService.getAllOcapiProducts(GB);

		pandoraService.createExcelFile(ocapiProducts, d365Products, "eComm_D365_Compare");
		return "/compareD365AndOcapi";
	}
}
