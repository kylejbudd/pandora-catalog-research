package com.madmobile.pandora.repositories;

import com.madmobile.pandora.dto.ProductItemDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("kwiRepository")
public interface KwiRepository extends JpaRepository<ProductItemDto, Long> {

}
