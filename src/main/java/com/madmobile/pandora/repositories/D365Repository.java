package com.madmobile.pandora.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.madmobile.pandora.dto.ProductItemDto;

public interface D365Repository extends JpaRepository<ProductItemDto, Long>{

}
