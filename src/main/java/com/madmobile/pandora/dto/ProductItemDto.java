package com.madmobile.pandora.dto;

import lombok.*;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Logger;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ProductItemDto  implements Comparable<ProductItemDto>{

	private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	String itemId;
	String description;


	@Override
	public int compareTo(@NotNull ProductItemDto dto) {
		return dto.getItemId().compareToIgnoreCase(this.getItemId());
	}
}
