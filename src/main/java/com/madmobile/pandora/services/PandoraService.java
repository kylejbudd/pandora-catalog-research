package com.madmobile.pandora.services;

import com.madmobile.pandora.dto.ProductItemDto;

import java.util.Set;

public interface PandoraService {

	final String BASE_OCAPI_SEARCH_URL_US = "https://development-usestore-pandora.demandware.net/s/en-US/dw/shop/v20_4/product_search?client_id=44a61cf1-e8ed-4f9c-a0f4-4cc633d476cd&locale=en-US&count=200&refine_1=";
	final String BASE_OCAPI_SEARCH_URL_GB = "https://development-pandora-pfsweb.demandware.net/s/en-GB/dw/shop/v20_4/product_search?client_id=44a61cf1-e8ed-4f9c-a0f4-4cc633d476cd&locale=en-GB&count=200&refine_1=";
	final String BASE_OCAPI_SEARCH_URL_GB_CATERGORY_BASE = "https://development-pandora-pfsweb.demandware.net/s/en-GB/dw/shop/v20_4/categories/root?client_id=44a61cf1-e8ed-4f9c-a0f4-4cc633d476cd&levels=10&locale=en-GB&expand=categories";

	final String KWI_BASE_URL = "https://njapiuat.kligerweiss.net/kwi/api/";
	final String KWI_AUTH_HEADER = "Basic cGFuZG9yYV90ZWFtOjIxMGYyYjUzLWU4YjctZjJlMS0yZjhkLTE0NTEyYmU2NDQ1Yw==";
	final String KWI_COOKIE = "JSESSIONID=BoLQ0wSDwtRFUQBn9VvE7Gq-7AIi7l8D.njapisvcuat01";

	final String DT365_BASE_URL = "https://pt01ret.sandbox.retail.dynamics.com/Commerce/Products/SearchByCategory(channelId=5637194091,catalogId=0,categoryId=";
	final String D365_CATEGORY_BASE_URL = "https://pt01ret.sandbox.retail.dynamics.com/Commerce/Categories/GetCategories?$top=250&api-version=7.3";
	final String D365_AUTH_TOKEN_URL = "https://login.microsoftonline.com/11b6d643-42be-4094-8799-22128db00ba2/oauth2/token";
	final String D365_AUTH_TOKEN_COOKIE = "fpc=Au493VRpcm1GvkQ3fbPtwY7n9h1BAQAAAIYJCdkOAAAA; stsservicecookie=estsfd; x-ms-gateway-slice=estsfd";
	final String D365_AUTH_TOKEN_CLIENTID = "257bbca2-c66d-4e76-a377-723298b77a28";
	final String D365_AUTH_TOKEN_CLIENT_SECRET = "-4j_5n1r87siX56lgoGr4~551_fodCCM3V";
	final String D365_AUTH_TOKEN_CLIENT_RESOURCE = "api://c736b9c1-8200-43ba-80b0-b3513708ca56";

	Set<ProductItemDto> getAllKWIProducts();

	Set<ProductItemDto> getAllD365DtoProduct();

	Set<ProductItemDto> getAllOcapiProducts(String locale);

	void createExcelFile(Set<ProductItemDto> set1, Set<ProductItemDto> set2, String fileName);
	
}
