package com.madmobile.pandora.services.impl;

import com.madmobile.pandora.dto.ProductItemDto;
import com.madmobile.pandora.services.PandoraService;
import com.madmobile.pandora.util.CreateExcelFile;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.SneakyThrows;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Service("pandoraServiceImpl")
public class PandoraServiceImpl implements PandoraService {

    private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @SneakyThrows
    @Override
    public Set<ProductItemDto> getAllKWIProducts() {

        Set<ProductItemDto> kwiProducts = new TreeSet<>();
        Unirest.setTimeouts(0, 0);
        int page = 1;

        while (true) {
            logger.log(Level.INFO, "Looping through page: " + page);
            HttpResponse<JsonNode> request = Unirest.get(KWI_BASE_URL + "product/getProduct?active=Y")
                    .queryString("query.page", page)
                    .header("Authorization", KWI_AUTH_HEADER)
                    .header("Cookie", KWI_COOKIE)
                    .asJson();

            if (request.getStatus() == HttpStatus.SC_OK) {
                addProductItemsToSet(kwiProducts, request, "result", "products", "alternate_sku", "report_description");

                logger.log(Level.INFO, "KWI products size: " + kwiProducts.size());
                page++;
            } else {
                break;
            }
        }
        logger.log(Level.INFO, "Finished looping through KWI products");

        return kwiProducts;
    }

    @Override
    public Set<ProductItemDto> getAllD365DtoProduct() {

        Unirest.setTimeouts(0, 0);
        Set<ProductItemDto> d365Products = new TreeSet<>();

        try {
            String idToken = "id_token " + getAuthToken();
            getD365Categories().forEach(element -> {
                try {
                    HttpResponse<JsonNode> response = Unirest
                            .get(DT365_BASE_URL + element + ")?$top=60&api-version=7.3")
                            .header("authorization", idToken)
                            .header("OUN", "00000450")
                            .asJson();

                    if (response.getStatus() == HttpStatus.SC_OK) {
                        addProductItemsToSet(d365Products, response, "", "value", "ItemId", "Description");
                        logger.log(Level.INFO, "D365 products size: " + d365Products.size());
                    }

                } catch (UnirestException unirestExc) {
                    logger.log(Level.WARNING, "UniException: " + unirestExc);
                }
            });

        } catch (UnirestException unirestExc) {
            logger.log(Level.WARNING, "UniException: " + unirestExc);
        }
        logger.log(Level.INFO, "Finish fetching D365 products");
        return d365Products;

    }

    @SneakyThrows
    @Override
    public Set<ProductItemDto> getAllOcapiProducts(String locale) {

        Set<ProductItemDto> eCommResults = new TreeSet<>();
        Map<String, Set<String>> refinementMap = new TreeMap<>();

        String ocapiBaseUrl = locale.equals(Locale.US.toLanguageTag()) ? BASE_OCAPI_SEARCH_URL_US : BASE_OCAPI_SEARCH_URL_GB;

        getOcapiProductsByPriceRefinement(eCommResults, ocapiBaseUrl, refinementMap);
        getOcapiProductsByCategoriesRefinement(eCommResults, ocapiBaseUrl);

        if (!refinementMap.isEmpty()) {
            getOcapiProductsByRemainingRefinements(eCommResults, refinementMap, ocapiBaseUrl);
        }

        logger.log(Level.INFO, "Finished collecting eCommResults. Size = " + eCommResults.size());

        return eCommResults;
    }

    private void getOcapiProductsByPriceRefinement(Set<ProductItemDto> eCommResults, String ocapiBaseUrl, Map<String, Set<String>> refinementMap) throws UnirestException {
        HttpResponse<JsonNode> response = Unirest.get(ocapiBaseUrl + "price=(0..9999999)").asJson();

        // Go ahead and collect the refinements off of the price.
        // Will loop through these later.
        JSONArray refinements = response.getBody().getObject().getJSONArray("refinements");
        createRefinementsMap(refinementMap, refinements);

        logger.log(Level.INFO, "Reading price pages");
        processResponse(eCommResults, response);
    }

    private void getOcapiProductsByCategoriesRefinement(Set<ProductItemDto> eCommResults, String ocapiBaseUrl) throws UnirestException {
        getOcapiCategories().forEach(category -> {
            logger.log(Level.INFO, "Reading category: " + category);
            try {
                HttpResponse<JsonNode> response = Unirest.get(ocapiBaseUrl + "cgid=" + category.replaceAll("\\s", "%20")).asJson();
                processResponse(eCommResults, response);
            } catch (UnirestException e) {
                e.printStackTrace();
            }
        });
    }

    private void getOcapiProductsByRemainingRefinements(Set<ProductItemDto> eCommResults, Map<String, Set<String>> refinementMap, String ocapiBaseUrl) throws UnirestException {

        refinementMap.forEach((refinement, values) -> values.forEach(value -> {
            logger.log(Level.INFO, "Reading refinement: " + refinement);
            try {
                HttpResponse<JsonNode> response = Unirest.get(ocapiBaseUrl + refinement + "=" + value.replaceAll("\\s", "%20")).asJson();
                logger.log(Level.INFO, "Reading Value: " + value);
                processResponse(eCommResults, response);
            } catch (UnirestException e) {
                e.printStackTrace();
            }
        }));
    }

    private void processResponse(Set<ProductItemDto> eCommResults, HttpResponse<JsonNode> response) throws UnirestException {
        if (response.getStatus() == HttpStatus.SC_OK) {
            String nextRequest = response.getBody().getObject().getInt("count") >= 200 ? response.getBody().getObject().getString("next") : null;

            if (response.getBody().getObject().getInt("total") > 0) {
                int page = 1;
                int totalPages = (response.getBody().getObject().getInt("total") / 200) + 1;
                logger.log(Level.INFO, "Processing page: " + page + " of: " + totalPages);

                addProductItemsToSet(eCommResults, response, "", "hits", "product_id", "product_name");
                while (nextRequest != null) {
                    page++;
                    response = Unirest.get(nextRequest).asJson();
                    nextRequest = response.getBody().getObject().getInt("count") >= 200 ? response.getBody().getObject().getString("next") : null;

                    logger.log(Level.INFO, "Processing page: " + page + " of: " + totalPages);
                    addProductItemsToSet(eCommResults, response, "", "hits", "product_id", "product_name");
                }
            }
        }
    }

    private void createRefinementsMap(Map<String, Set<String>> refinementMap, JSONArray refinements) {
        
        refinements.forEach(   			
        				item -> { 
        						JSONObject obj = (JSONObject) item;
        						String attribute_id = obj.getString("attribute_id");
        						
        						 // cgid and price will be used in the two previous collection methods
        						if ( !attribute_id.equals("cgid") && !attribute_id.equals("price") ) {
        							
        							Set<String> valuesSet = new TreeSet<>();
        							JSONArray values = obj.getJSONArray("values");
        							
        							values.forEach( ele -> { 
        													JSONObject object = (JSONObject) ele;
        													valuesSet.add(object.getString("value"));
        													}    
        										 );
        							refinementMap.put(attribute_id, valuesSet);
        						}
        			});
        
    }

    @Override
    public void createExcelFile(Set<ProductItemDto> ocapiProducts, Set<ProductItemDto> posProducts, String fileName) {

        CreateExcelFile excelFile = new CreateExcelFile();
        Set<ProductItemDto> missingProducts = getElementsInPosNotinOCAPI(ocapiProducts, posProducts);

        List<Set<ProductItemDto>> productSets = new ArrayList<Set<ProductItemDto>>();
        productSets.add(missingProducts);
        productSets.add(ocapiProducts);
        productSets.add(posProducts);
        try {
            excelFile.createWorkbook(productSets, fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ocapi: Elements in OCAPI
    // pos: Elements in POS
    // return: elements in pos (POS) but not in eCommerce (OCAPI)
    private Set<ProductItemDto> getElementsInPosNotinOCAPI(Set<ProductItemDto> ocapi, Set<ProductItemDto> pos) {

        return pos.stream().filter(p -> ocapi.stream().noneMatch(o -> p.getItemId().equalsIgnoreCase(o.getItemId()))).collect(Collectors.toSet());
    }

    private String getAuthToken() throws UnirestException {

        String accessToken = null;
        HttpResponse<JsonNode> response = Unirest 
                .post(D365_AUTH_TOKEN_URL)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Cookie", D365_AUTH_TOKEN_COOKIE)
                .field("grant_type", "client_credentials").field("client_id", D365_AUTH_TOKEN_CLIENTID)
                .field("client_secret", D365_AUTH_TOKEN_CLIENT_SECRET)
                .field("resource", D365_AUTH_TOKEN_CLIENT_RESOURCE).asJson();

        if (response.getStatus() == HttpStatus.SC_OK) {
            JSONObject responseObject = response.getBody().getObject();
            accessToken = responseObject.getString("access_token");

        } else {
            logger.log(Level.SEVERE, "Unable to authenticate");
            throw new UnirestException("Unable to authenticate");
        }
        return accessToken;
    }

    private List<String> getOcapiCategories() throws UnirestException {
        HttpResponse<JsonNode> response = Unirest.get(BASE_OCAPI_SEARCH_URL_GB_CATERGORY_BASE).asJson();
        JSONArray categoryArray = response.getBody().getObject().getJSONArray("categories");
        List<String> categoryIdList = IntStream.range(0, categoryArray.length()).mapToObj(i -> categoryArray.getJSONObject(i).getString("id")).collect(Collectors.toList());

        return categoryIdList;
    }

    private Set<Long> getD365Categories() throws UnirestException {
        String idToken = "id_token " + getAuthToken();
        Set<Long> categoryIds = new HashSet<Long>();

        HttpResponse<JsonNode> response = Unirest.post(D365_CATEGORY_BASE_URL)
                								.header("authorization", idToken).header("OUN", "00000450").header("Content-Type", "application/json")
                								.body("{\n    \"channelId\": 5637194091\n}").asJson();

        if (response.getStatus() == HttpStatus.SC_OK) {

            JSONObject responseObject = response.getBody().getObject();
            JSONArray products = responseObject.getJSONArray("value");

            products.forEach(element -> {
                JSONObject obj = (JSONObject) element;
                categoryIds.add(obj.getLong("ParentCategory"));
            });

        }

        return categoryIds;
    }

    private void addProductItemsToSet(Set<ProductItemDto> resultSet, HttpResponse<JsonNode> response, String result, String product, String id, String description) {

        JSONObject responseObject = response.getBody().getObject();
        JSONArray products;
        if (result.length() > 0)
            products = responseObject.getJSONObject(result).getJSONArray(product);
        else
            products = responseObject.getJSONArray(product);

        products.forEach( element -> { JSONObject obj = (JSONObject) element;
            ProductItemDto itemDto = ProductItemDto.builder()
                    .itemId(obj.getString(id))
                    .description(obj.getString(description)).build();
            resultSet.add(itemDto);
        });
    }


}
