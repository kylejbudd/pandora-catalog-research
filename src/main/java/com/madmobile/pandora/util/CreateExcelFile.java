package com.madmobile.pandora.util;

import com.madmobile.pandora.dto.ProductItemDto;
import lombok.AllArgsConstructor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

@AllArgsConstructor
public class CreateExcelFile {

    private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public void createWorkbook(List<Set<ProductItemDto>> products, String fileName) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        Map<XSSFSheet, Set<ProductItemDto>> sheetSetMap = prepareWorkBook(products, workbook);

        for (Map.Entry<XSSFSheet, Set<ProductItemDto>> entry : sheetSetMap.entrySet()) {
            int rowCount = 0;
            createHeader(entry.getKey(), workbook);
            for (ProductItemDto product : entry.getValue()) {
                Row row = entry.getKey().createRow(++rowCount);
                logger.log(Level.INFO, "Writing row: " + rowCount + " of: " + entry.getValue().size() + " on sheet: " + entry.getKey().getSheetName());

                Cell upcCell = row.createCell(0);
                upcCell.setCellValue(product.getItemId());

                Cell descriptionCell = row.createCell(1);
                descriptionCell.setCellValue(product.getDescription());
            }
        }

        String home = System.getProperty("user.home");
        String pathName = home + "/Downloads/" + fileName + ".xlsx";

        logger.log(Level.INFO, "Path to file: " + pathName);

        File file = new File(pathName);

        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
        } catch (FileNotFoundException exception) {
            logger.log(Level.WARNING, "Exception creating file: " + exception);
        } finally {
            logger.log(Level.INFO, "Workbook created");
            workbook.close();
        }
    }

    private Map<XSSFSheet, Set<ProductItemDto>> prepareWorkBook(List<Set<ProductItemDto>> products, XSSFWorkbook workbook) {
        Map<XSSFSheet, Set<ProductItemDto>> sheetSetMap = new HashMap<>();

        XSSFSheet comparedSheet = workbook.createSheet("In_POS_Not_In_eComm");
        XSSFSheet ocapiSheet = workbook.createSheet("eComm");
        XSSFSheet posSheet = workbook.createSheet("POS");

        sheetSetMap.put(comparedSheet, products.get(0));
        sheetSetMap.put(ocapiSheet, products.get(1));
        sheetSetMap.put(posSheet, products.get(2));

        return sheetSetMap;
    }

    private void createHeader(XSSFSheet sheet, XSSFWorkbook workbook) {
        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue("ID");
        headerRow.createCell(1).setCellValue("Description");
    }

}
